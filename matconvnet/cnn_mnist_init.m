function net = cnn_mnist_init(varargin)
% CNN_MNIST_LENET Initialize a CNN similar for MNIST
opts.useBnorm = true ;
opts = vl_argparse(opts, varargin) ;

rng('default');
rng(0) ;
load('data/mnist-baseline/classNumbers.mat');

% 2nd convolution weight 
conv2weights = [1 0 0 0 1 1 1 0 0 1 1 1 1 0 1 1; ...
                1 1 0 0 0 1 1 1 0 0 1 1 1 1 0 1; ...
                1 1 1 0 0 0 1 1 1 0 0 1 0 1 1 1; ...
                0 1 1 1 0 0 1 1 1 1 0 0 1 0 1 1; ...
                0 0 1 1 1 0 0 1 1 1 1 0 1 1 0 1; ...
                0 0 0 1 1 1 0 0 1 1 1 1 0 1 1 1];
conv2weights = reshape(conv2weights, 1, 1, 6, 16);

f = 1/100;
% CONVOLUTION 1
net.layers = {};
net.layers{end+1} = struct('type', 'conv', ...
                       'weights', {{f*randn(5,5,1,6, 'single'), zeros(1, 6, 'single')}}, ...
                       'stride', 1, ...
                       'pad', 2);                   

% SUBSAMPLING 1
net.layers{end+1} = struct('type', 'pool', ...
                       'method', 'max', ...
                       'pool', [2, 2], ...
                       'stride', 2, ...
                       'pad', 0);   
                   
% CONVOLUTION 2
net.layers{end+1} = struct('type', 'conv', ...
                       'weights', {{bsxfun(@times, conv2weights, f*randn(5,5,6,16, 'single')), zeros(1, 16, 'single')}}, ...
                       'stride', 1, ...
                       'pad', 0);                   

% SUBSAMPLING 2                   
net.layers{end+1} = struct('type', 'pool', ...
                       'method', 'max', ...
                       'pool', [2, 2], ...
                       'stride', 2, ...
                       'pad', 0);                
% CONVOLUTION 3
% Fully connected layer
net.layers{end+1} = struct('type', 'conv', ...
                       'weights', {{f*randn(5,5,16,120, 'single'), zeros(1,120, 'single')}}, ...
                       'stride', 1, ...
                       'pad', 0);
                   
% CONVOLUTION 4
% Fully connected layer
net.layers{end+1} = struct('type', 'conv', ...
                       'weights', {{f*randn(1,1,120,84, 'single'), zeros(1,84, 'single')}}, ...
                       'stride', 1, ...
                       'pad', 0);

% HYPERBOLIC TANGENT                   
net.layers{end+1} = struct('type', 'custom', ...
                           'forward', @htangent, ...
                           'backward', @htangentb);

% EUCLIDEAN RADIAL BASIS                       
net.layers{end+1} = struct('type', 'custom', ...
                           'p', 2, ...
                           'noRoot', true, ...
                           'epsilon', 0,...
                           'class', classNumbers, ...
                           'forward', @ERB, ...
                           'backward', @ERBb);
                          
% OUTPUT
net.layers{end+1} = struct('type', 'softmaxloss');
    
% optionally switch to batch normalization
if opts.useBnorm
  net = insertBnorm(net, 1) ;
  net = insertBnorm(net, 4) ;
  net = insertBnorm(net, 7) ;
end

% --------------------------------------------------------------------
function net = insertBnorm(net, l)
% --------------------------------------------------------------------
assert(isfield(net.layers{l}, 'weights'));
ndim = size(net.layers{l}.weights{1}, 4);
layer = struct('type', 'bnorm', ...
               'weights', {{ones(ndim, 1, 'single'), zeros(ndim, 1, 'single')}}, ...
               'learningRate', [1 1], ...
               'weightDecay', [0 0]) ;
net.layers{l}.biases = [] ;
net.layers = horzcat(net.layers(1:l), layer, net.layers(l+1:end)) ;

% --------------------------------------------------------------------
function output = htangent(layer, in, out)
% --------------------------------------------------------------------
output = out;
output.x = 1.7159*tanh(in.x*(2/3));

% --------------------------------------------------------------------
function output = htangentb(layer, in, out)
% --------------------------------------------------------------------
output = in;
output.dzdx = out.dzdx.*(1 - (in.x.*in.x));

% --------------------------------------------------------------------
function output = ERB(layer, in, out)
% --------------------------------------------------------------------

output = out;
for i = 1:size(layer.class, 4)
    d = bsxfun(@minus, in.x, layer.class(:,:,:,i));
    output.x(1,1,i,:) = sum(d.*d,3) ; 
end

% --------------------------------------------------------------------
function output = ERBb(layer, in, out)
% --------------------------------------------------------------------

output = in;
a = zeros(1,10,84,size(in.x,4));
for i = 1:10
    a(:,i,:,:) = bsxfun(@times, 2*out.dzdx(:,:,i,:), bsxfun(@minus, in.x, layer.class(:,:,:,i)));
end
output.dzdx = sum(a,2);
