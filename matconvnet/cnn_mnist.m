function [net, info, imdb] = cnn_mnist(varargin)
% CNN_MNIST  Demonstrated MatConNet on MNIST

setup;

opts.expDir = fullfile('data','mnist-baseline') ;
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.batchFunction = @getBatchWithDistortion;
opts.dataDir = fullfile('data','mnist') ;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
opts.netPath = fullfile(opts.expDir, 'net.mat');
opts.infoPath = fullfile(opts.expDir, 'info.mat');
opts.classNumbersPath = fullfile(opts.expDir, 'classNumbers.mat');
opts.useBnorm = false ;
opts.fractionValidation = 0.1; %Fraction of traing set to use as validation
opts.train.batchSize = 100 ;
opts.train.numEpochs = 20;
opts.train.continue = true ;
opts.train.gpus = [] ; %Set to empty array if you want to run on CPU
opts.train.learningRate = [0.0005*ones(1,2) 0.0002*ones(1,3) 0.0001*ones(1,3) 0.00005*ones(1,4) 0.00001*ones(1,8)] ;
opts.train.weightDecay = 0.02;
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

if exist(opts.imdbPath, 'file')
  imdb = load(opts.imdbPath) ;
else
  imdb = getMnistImdb(opts) ;
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end

if exist(opts.classNumbersPath, 'file')
    classNumbers = load(opts.classNumbersPath);
else
    classNumbers = getClassNumbers();
    mkdir(opts.expDir);
    save(opts.classNumbersPath, 'classNumbers');
end

net = cnn_mnist_init('useBnorm', opts.useBnorm) ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------
if exist(opts.netPath, 'file') && exist(opts.infoPath, 'file')
  net = load(opts.netPath) ;
  info = load(opts.infoPath) ;
else
  [net, info] = cnn_train(net, imdb, opts.batchFunction , opts.train);
  net.layers(end) = [] ;
  mkdir(opts.expDir) ;
  save(opts.netPath, '-struct', 'net') ;
  save(opts.infoPath, '-struct', 'info') ;
end

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithNormalization(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
im(im>0)=1.175;
im(im<0)=-0.1;
labels = imdb.images.labels(1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithDistortion(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
for i=1:size(batch)
   im(:,:,:,i) = lensdistort(im(:,:,:,i), 1); 
end
labels = imdb.images.labels(1,batch) ;
% --------------------------------------------------------------------
function classNumbers = getClassNumbers()
% --------------------------------------------------------------------
array = imread('font7x12.png');
classNumbers = zeros(84,10);
for i = 1:10
    for j = 1:12
        classNumbers(((7*(j-1))+1):((7*(j-1))+7),i) = array(12+j, ((7*(i-1))+1):((7*(i-1))+7));
    end
end

for i = 1:84
    for j = 1:10
        if classNumbers(i,j) > 0
            classNumbers(i,j) = 1;
        else
            classNumbers(i,j) = -1;
        end
    end
end
classNumbers = reshape(classNumbers,1,1,84,10);

% --------------------------------------------------------------------
function imdb = getMnistImdb(opts)
% --------------------------------------------------------------------
% Preapre the imdb structure, returns image data with mean image subtracted
files = {'train-images-idx3-ubyte', ...
         'train-labels-idx1-ubyte', ...
         't10k-images-idx3-ubyte', ...
         't10k-labels-idx1-ubyte'} ;

if ~exist(opts.dataDir, 'dir')
  mkdir(opts.dataDir) ;
end

for i=1:4
  if ~exist(fullfile(opts.dataDir, files{i}), 'file')
    url = sprintf('http://yann.lecun.com/exdb/mnist/%s.gz',files{i}) ;
    fprintf('downloading %s\n', url) ;
    gunzip(url, opts.dataDir) ;
  end
end

f=fopen(fullfile(opts.dataDir, 'train-images-idx3-ubyte'),'r') ;
x1=fread(f,inf,'uint8');
fclose(f) ;
x1=permute(reshape(x1(17:end),28,28,60e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 't10k-images-idx3-ubyte'),'r') ;
x2=fread(f,inf,'uint8');
fclose(f) ;
x2=permute(reshape(x2(17:end),28,28,10e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 'train-labels-idx1-ubyte'),'r') ;
y1=fread(f,inf,'uint8');
fclose(f) ;
y1=double(y1(9:end)')+1 ;

f=fopen(fullfile(opts.dataDir, 't10k-labels-idx1-ubyte'),'r') ;
y2=fread(f,inf,'uint8');
fclose(f) ;
y2=double(y2(9:end)')+1 ;

%Build a array of training set numbers
training = ones(1,numel(y1));
%Get a fraction of those so that we can set them to validation. 
[~,sel] = vl_colsubset(training, opts.fractionValidation, 'random');
training(:,sel)=2; %Set the selection to 2 for validation.
set = [training 3*ones(1,numel(y2))];
data = single(reshape(cat(3, x1, x2),28,28,1,[]));
dataMean = mean(data(:,:,:,set == 1), 4);
data = bsxfun(@minus, data, dataMean) ;

imdb.images.data = data ;
imdb.images.data_mean = dataMean;
imdb.images.labels = cat(2, y1, y2) ;
imdb.images.set = set ;
imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:9,'uniformoutput',false) ;
