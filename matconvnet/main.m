[net, info, imdb] = cnn_mnist();

% --------------------------------------------------------------------
%                                                                 Test
% --------------------------------------------------------------------
testIndexs = find(imdb.images.set==3);
im = imdb.images.data(:,:,:,testIndexs) ;
labels = imdb.images.labels(1,testIndexs) ;
res = vl_simplenn(net, im);
scores = squeeze(gather(res(end).x)) ;
[bestScore, best] = max(scores) ;
%Fill with 1s because we are optimistic most will be right.
errors = ones(1,numel(labels)); 
%Set to 0 all the entries whos best ~= label, aka we got it wrong.
errors(best ~= labels) = 0; 
numErrors = length(errors(errors==0));
numTotal = length(labels);
fprintf('%d/%d wrongly identified for error of: %.3f\n',... 
numErrors, numTotal, (numErrors/numTotal)*100);