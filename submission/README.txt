CMPUT 399 MNIST Data Classification. 
Team SGD: James Hodgson, William Hodgson, Jingjiao Ni and Ji Yang

+---+
|CNN|
+---+
Our LeNet-5 Convolutional Neural network is made with the help of the Matconvnet library. All code is inside the CNN folder.

It is split over 5 files:
	cnn_mnist.m: a modified Matconvnet file that trains a LeNet-5 network
	cnn_mnist_init.m: a modified Matconvnet file that initializes the LeNet-5 network
	main.m: trains the network then calculates the test error
	setup.m: all setup configuration including compiling if neccesary
	lensdistort.m: function for distorting the images. Taken from: http://www.mathworks.com/matlabcentral/fileexchange/37980-barrel-and-pincushion-lens-distortion-correction/content/lensdistort/lensdistort.m

Notes:
	- network options (eg: numEpochs, learningRate) are in cnn_mnist.m
	- to change input from distorted images to non-distorted:
		set opts.batchFunction = @getBatch on line 9
	- to change input from distorted images to normalized images:
		set opts.batchFunction = @getBatchWithNormalization
	- Matconvnet and vlfeat are included in this code however they may need re-compiling for your platform.
	- The custom layers on our net do not have GPU support. This project thus cannot be run with matconvnet cpu support on.
	
+---+
|SVM|
+---+
Due to the size of the SVM code we have put it on drop box. It can be found @ https://drive.google.com/a/ualberta.ca/file/d/0B_RepNiWR4bwczVVMmRhYXcxTzQ/view